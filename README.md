# Indexeddb #

**The indexeddb is a new HTML5 concept** to store the data inside user's browser. indexeddb is more power than local storage and useful for applications that requires to store large amount of the data. 


## Why to use indexeddb? ##
The W3C has announced that the Web SQL database is a deprecated local storage specification so web developer should not use this technology any more. indexeddb is an alternative for web SQL data base and more effective than older technologies.


https://www.tutorialspoint.com/html5/html5_indexeddb.htm