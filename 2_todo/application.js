(function() {
 
  var db;

  addEventListener('click', onClick);
  addEventListener('submit', onSubmit);

  databaseOpen(function() {        
      databaseTodosGet();
  });
 

  function onSubmit(e) {
    e.preventDefault();
    var input = document.querySelector('input');
    var transaction = db.transaction(['todo'], 'readwrite');
    var store = transaction.objectStore('todo');

    var x = new Object();
    x.text = input.value;
    x.timeStamp = Date.now();
    
    var request = store.put(x);

    input.value = '';

    request.onerror = function(e) {
     //console.log("INSERT ERROR");
    };

    request.onsuccess = function(e) {
     //console.log("INSERT onsuccess");
    };

    transaction.oncomplete = function(e) {
     console.log("transaction oncomplete");
    };

    databaseTodosGet(function() { 
    });
   

  }


  function onClick(e) {
    console.log(e.target.getAttribute('id')); 
    if (e.target.hasAttribute('id')) {
      databaseTodosDelete(parseInt(e.target.getAttribute('id')), function() {
        databaseTodosGet();
      });
    }
  }
  
 
  function databaseOpen(callback) {
    // Open a database, specify the name and version
    var version = 1;
    var request = indexedDB.open('todos', version);
 
    // Run only when create or change version
    request.onupgradeneeded = function(e) {
      db = e.target.result;
      var store = db.createObjectStore('todo', { keyPath: 'timeStamp' });

  
      var x = new Object();
      x.text = "hola mundo";
      x.timeStamp = Date.now();
      
      var request = store.put(x);
      request.onsuccess = function(e) {
        console.log("default compleate");
      }
    };

    //IF EVERYTHING IS
    request.onsuccess = function(e) {
      db = e.target.result;
      callback();
    };

    //PRINT ERROR
    request.onerror = function(e) {
       console.error('An IndexedDB Error : ' + event.target.errorCode);
       callback();  
    };

  }
 
 
  function databaseTodosAdd(text, callback) {
    var transaction = db.transaction(['todo'], 'readwrite');
    var store = transaction.objectStore('todo');
    
    var request = store.put({
      text: text,
      timeStamp: Date.now()
    });
 
    transaction.oncomplete = function(e) {
      callback();
    };
    
  }

  function databaseTodosGet() {

    var ul = document.querySelector('ul');
    var transaction = db.transaction(['todo'], 'readonly');
    var objectStore = transaction.objectStore('todo');
    var request = objectStore.openCursor();
    var data = [];


    request.onsuccess = function(e){
      var result = e.target.result; 
      if(result) {
          data.push(result.value);
          result.continue();
      } else {
        var html = '';
        data.forEach(function(item) {
          html += '<li id="'+item.timeStamp+'">'+item.text+'</li>'; 
        });
        ul.innerHTML = html;
      }     
    }
    transaction.oncomplete = function(e) {
        var cursor = e.target.result;
    }

  }



  function databaseTodosDelete(id, callback) {
    var transaction = db.transaction(['todo'], 'readwrite');
    var store = transaction.objectStore('todo');
    var request = store.delete(id);
    
    transaction.oncomplete = function(e) {
      console.log("Delete oncomplete");
      callback();
    };
    
    request.onsuccess = function(e) {
       console.log("Delete onsuccess");
       callback();  
    };

    request.onerror = function(e) {
       console.error('Delete onerror: ' + event.target.errorCode);
       callback();  
    };
  }

 
}());

 